package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq" // Обратите внимание на анонимный импорт
	"gitlab.com/legaru/proxyclient/client"
	"log"
	"net/http"
	"time"
)

func main() {
	// Инициализация конфигурации
	cfg := &client.Config{
		RetryCount:       3,
		ProxyGroupNumber: 0,
		ProxyType:        "http",  // или "socks"
	}

	connect, err := sql.Open("postgres", "dsn")

	if err != nil {
		log.Fatal( fmt.Errorf("[sql.Open] %v", err))
	}

	if err := connect.Ping(); err != nil {
		log.Fatal(fmt.Errorf("[connect.Ping] %v", err))
	}
	if err != nil {
		log.Fatal("Ошибка sql.Open:", err)
	}

	// Создание клиента прокси
	proxyClient := client.NewProxyClient(cfg, connect)
	proxyClient.UpdateProxies()

	if err != nil {
		log.Fatal("Ошибка при загрузке списка проксей:", err)
	}

	// Создание HTTP-запроса
	req, err := http.NewRequest("GET", "https://example.com/", nil)
	if err != nil {
		log.Fatalf("Ошибка при создании запроса: %v", err)
	}

	// Выполнение запроса через прокси
	response, err := proxyClient.ExecuteRequest(req)
	if err != nil {
		log.Fatalf("Ошибка при выполнении запроса через прокси: %v", err)
	}
	defer response.Body.Close()

	fmt.Println(response.StatusCode)


	// Обработка ответа
	// ...

	log.Println("Запрос успешно выполнен.")
}


const timeout = 15 * time.Minute

type PostgresStoreRepository struct {
	connect *sql.DB
	conf    *client.Config
}
