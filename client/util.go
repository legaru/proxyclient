package client

func IsErrorResponse(statusCode int, errorCodes []int) bool {
	for _, code := range errorCodes {
		if statusCode == code {
			return true
		}
	}
	return false
}
