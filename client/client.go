package client

import (
	"database/sql"
	"fmt"
	netproxy "golang.org/x/net/proxy"
	"log"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

type ProxyClient struct {
	Config       *Config
	DB           *sql.DB
	Proxies      []Proxy
	currentIndex int // Индекс текущего прокси в списке
}

func NewProxyClient(cfg *Config, db *sql.DB) *ProxyClient {
	return &ProxyClient{
		Config:  cfg,
		DB:      db,
		Proxies: []Proxy{},
	}
}

func (pc *ProxyClient) ExecuteRequest(req *http.Request) (*http.Response, error) {
	var lastError error
	retryDelay := 1 * time.Second // Начальная задержка повторной попытки

	for i := 0; i < pc.Config.RetryCount; i++ {
		pr := pc.getNextProxy()
		if pr == nil {
			log.Println("Нет доступных проксей")
			return nil, lastError
		}

		var httpClient *http.Client
		switch pc.Config.ProxyType {
		case "http":
			proxyHttpPort := strconv.Itoa(pr.ProxyHttpPort)
			proxyURL, _ := url.Parse("http://" + pr.Username + ":" + pr.Password + "@" + pr.ProxyIp + ":" + proxyHttpPort)
			httpClient = &http.Client{
				Transport: &http.Transport{
					Proxy: http.ProxyURL(proxyURL),
				},
				Timeout: 10 * time.Second,
			}

		case "socks":
			proxySocksPort := strconv.Itoa(pr.ProxySocksPort)
			dialer, _ := netproxy.SOCKS5("tcp", pr.ProxyIp+":"+proxySocksPort, &netproxy.Auth{
				User:     pr.Username,
				Password: pr.Password,
			}, netproxy.Direct)
			httpClient = &http.Client{
				Transport: &http.Transport{
					Dial: dialer.Dial,
				},
				Timeout: 10 * time.Second,
			}

		default:
			log.Fatalf("Не переданный в конфиге или неподдерживаемый тип прокси: %s", pc.Config.ProxyType)
		}

		response, err := httpClient.Do(req)
		if err != nil {
			log.Printf("Ошибка при выполнении запроса: %v", err)

			if nErr, ok := err.(net.Error); ok && nErr.Timeout() {
				log.Println("Ошибка таймаута, повторная попытка...")
			} else if ok && nErr.Temporary() {
				log.Println("Временная ошибка сети, повторная попытка...")
			} else {
				return nil, err // Для несетевых ошибок прерываем выполнение
			}

			lastError = err
			time.Sleep(retryDelay)
			retryDelay *= 2 // Экспоненциальный откат
			continue
		}

		if response.StatusCode == http.StatusOK {
			return response, nil
		} else {
			// Проверка, является ли код ответа ошибкой в соответствии с ErrorHTTPCodes
			if IsErrorResponse(response.StatusCode, pc.Config.ErrorHTTPCodes) {
				log.Printf("Ошибка HTTP: %d", response.StatusCode)
				lastError = fmt.Errorf("ошибка HTTP: %d", response.StatusCode)
				pr.LastError = time.Now()
				continue
			}
		}
	}
	return nil, lastError
}

func (pc *ProxyClient) getNextProxy() *Proxy {
	if len(pc.Proxies) == 0 {
		return nil // Возврат nil, если список проксей пуст
	}

	// Получение текущего прокси
	proxy := &pc.Proxies[pc.currentIndex]

	// Увеличение индекса для следующего вызова
	pc.currentIndex = (pc.currentIndex + 1) % len(pc.Proxies)

	return proxy
}

func (pc *ProxyClient) UpdateProxies() error {
	groupNumber := pc.Config.ProxyGroupNumber
	proxies, err := FetchProxies(pc.DB, groupNumber)
	if err != nil {
		// Обработка ошибок
		return err
	}
	pc.Proxies = proxies
	return nil
}
