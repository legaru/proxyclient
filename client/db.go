package client

import (
	"database/sql"
	"log"
)

func FetchProxies(pc *sql.DB, groupNumber int) ([]Proxy, error) {
	query := "SELECT proxy_ip, proxy_port_http, proxy_port_socks, proxy_login, proxy_password FROM proxies WHERE group_number = $1 AND check_error_code = 200  AND provider_name = 'provider'"
	rows, err := pc.Query(query, groupNumber)

	if err != nil {
		log.Printf("Ошибка при получении проксей из БД: %v", err)
		return nil, err
	}
	defer rows.Close()

	var Proxies []Proxy
	for rows.Next() {
		var p Proxy
		if err := rows.Scan(&p.ProxyIp, &p.ProxyHttpPort,  &p.ProxySocksPort, &p.Username, &p.Password); err != nil {
			log.Printf("Ошибка при чтении строки: %v", err)
			continue
		}
		Proxies = append(Proxies, p)
	}

	if err := rows.Err(); err != nil {
		log.Printf("Ошибка при обработке строк: %v", err)
		return nil, err
	}

	log.Printf("Получен список проксей: %v", Proxies)
	return Proxies, nil
}
