package client

import (
	"fmt"
	ym "gopkg.in/yaml.v3"
)

type Config struct {
	RetryCount       int
	ErrorHTTPCodes   []int
	ProxyGroupNumber int
	ProxyType        string
}

func (c *Config) Config(bytes []byte) error {
	err := ym.Unmarshal(bytes, c)
	if err != nil {
		return fmt.Errorf("[yaml.Unmarshal] %w", err)
	}
	return nil
}

