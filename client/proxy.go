package client

import (
	"time"
)

type Proxy struct {
	ProxyIp        string
	ProxyHttpPort  int
	ProxySocksPort int
	Username       string
	Password       string
	LastError      time.Time
}

