module gitlab.com/legaru/proxyclient

go 1.20

require (
	golang.org/x/net v0.19.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/lib/pq v1.10.9 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
